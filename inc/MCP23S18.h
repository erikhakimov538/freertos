#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "MDR32F9Qx_uart.h"
#include "system_MDR1986BE4.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_ssp.h"

/* адреса регистров для порта A */
#define  DIRA             (0x00)
#define IPOLA             (0x02)
#define GPINTENA          (0x04)
#define DEFVALA           (0x06)
#define INTCONA           (0x08)
#define IOCONA            (0x0A)
#define GPPUA             (0x0C)
#define INTFA             (0x0E)
#define INTCAPA           (0x10)
#define GPIOA             (0x12)
#define OLATA             (0x14)
/* адреса регистров для порта B */
#define DIRB              (0x01)
#define IPOLB             (0x03)
#define GPINTENB          (0x05)
#define DEFVALB           (0x07)
#define INTCONB           (0x09)
#define IOCONB            (0x0B)
#define GPPUB             (0x0D)
#define INTFB             (0x0F)
#define INTCAPB           (0x11)
#define GPIOB             (0x13)
#define OLATB             (0x15)
#define IOCON_default     (0x0A)
    
/* значения для настройки регистров */
#define DIRB_cfg          (0x00)
#define IPOLB_cfg         (0x00)
#define GPINTENB_cfg      (0x00)
#define DEFVALB_cfg       (0x00)
#define INTCONB_cfg       (0x00)
#define IOCONB_cfg        (0x00)
#define GPPUB_cfg         (0x00)