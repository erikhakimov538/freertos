#ifndef __MISC_H__
#define __MISC_H__


typedef unsigned char bool;
#define true	1
#define false	0
//---------------------------------------------------------------------------------------------------
typedef struct{
	unsigned int q;		// quotient 
	unsigned int r;		// remainder
} uidiv_t;

uidiv_t uidiv(unsigned int dividend, unsigned int divisor);

unsigned int uint2text(unsigned int val, unsigned char* str, unsigned int max_cnt);
//---------------------------------------------------------------------------------------------------

#define PLL_Mul_ 2
#define FCLK_ 16

#define CPU_CLK_ ((FCLK_) * (PLL_Mul_))
#define	DELAY(d) ((CPU_CLK_) * (d) / 32 + 1)
#define   DELAY_10NS			DELAY(10)
#define 	DELAY_160NS 		DELAY(160)
#define 	DELAY_226NS 		DELAY(226)

#define		DELAY_10US		DELAY(10)
#define		DELAY_15US		DELAY(15)
#define		DELAY_50US		DELAY(50)
#define		DELAY_100US		DELAY(100)
#define		DELAY_200US		DELAY(200)
#define		DELAY_500US		DELAY(500)
#define   DELAY_2SEC		DELAY(2000000)
void delay(unsigned int delay);

#define BM(n)		(1ul << (n))

#define LOBYTE(w)         (unsigned char)(w)
#define HIBYTE(w)         (unsigned char)((w) >> 8)
#define MAKEWORD(lo, hi)  ((unsigned short)(unsigned char)(lo) | ((unsigned short)(unsigned char)(hi) << 8))

#define HINIBBLE(b)			(((unsigned char)(b) & 0xF0) >> 4)
#define LONIBBLE(b)			((unsigned int)(b) & 0x0F)

#define EXCHANGE(a) (((unsigned short)(a) >> 8) | (((unsigned short)(a) & 0xFF) << 8))							 

#define NO	((unsigned short)(-1))

unsigned int CalcSum(void* buf, unsigned int size);
unsigned short CalcHeaderCS(void* buf, unsigned int size);

#endif
