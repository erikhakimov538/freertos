#ifndef __UART_H__
#define __UART_H__

//#include "MDR32F9Qx_uart.h"
#include "misc.h"

typedef struct{
	unsigned int speed;
	unsigned short bit_cnt;
	unsigned short stop_bits; 
	unsigned short parity;
} TUARTSettings;

void uart_Init(unsigned int speed, unsigned short bit_cnt, unsigned short stop_bits, unsigned short parity);
bool uart_SendByte(char byte);
bool uart_GetByte(char* pb);
bool uart_SendStr(char* str);
bool uart_SendBuf(char* buf, unsigned int size);
void uart_IRQ(void);

void uart_GetSettings(TUARTSettings* settings);
void uart_SetSettings(TUARTSettings* settings);

void uart_Test(void);

#endif
