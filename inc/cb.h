#ifndef __CB_H__
#define __CB_H__

#include "misc.h"

// ��������� �����
typedef struct{
	unsigned int	head;
	unsigned int	tail;
	unsigned int	cnt;
	char*					pbuf;
	unsigned int	buf_size; 
} TCB;

void cb_Init(TCB* pcb, char* pbuf, unsigned int buf_size);
void cb_Reset(TCB* pcb);
bool cb_AddByte(TCB* pcb, char b);
bool cb_GetByte(TCB* pcb, char* pb);
unsigned int cb_GetByteCnt(TCB* pcb);
bool cb_IsEmpty(TCB* pcb);


#endif
