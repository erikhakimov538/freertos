INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
CMAKE_FORCE_C_COMPILER(arm-none-eabi-gcc GNU)
CMAKE_FORCE_CXX_COMPILER(arm-none-eabi-g++ GNU)

SET(CMAKE_OBJCOPY "arm-none-eabi-objcopy")
SET(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/src/MDR1986BE4.ld)
SET(COMMON_FLAGS "-mcpu=cortex-m0 -mthumb -mthumb-interwork -ffunction-sections -fdata-sections -Wall -Wextra -Wshadow  \
 -Wredundant-decls -Wno-missing-field-initializers -pipe")

SET(CMAKE_CXX_FLAGS "${COMMON_FLAGS} -std=c++11")
SET(CMAKE_C_FLAGS "${COMMON_FLAGS} -std=gnu99 -save-temps")

SET(CMAKE_EXE_LINKER_FLAGS "-mcpu=cortex-m0 -Wl,-gc-sections -T ${LINKER_SCRIPT} --specs=nosys.specs --specs=nano.specs \
 -ffreestanding -Wl,-u,_printf_float -Wl,-u,_scanf_float -Wl,-u,vfprintf -Wl,-u,cosf -Wl,-u,sinf -lm")

