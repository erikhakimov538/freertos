/* first asm code */
     .syntax	unified
	.cpu cortex-m0
     
     .text

     
     .thumb
     .thumb_func
     .align 2
     .globl   delay_asm
     .type    delay_asm, %function
     
delay_asm:
     loop:
     nop
     subs r0, 1     
     bne loop
     bx lr


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   led_on_asm
     .type    led_on_asm, %function 
/*r0 - base port adress r1 - pin mask*/
led_on_asm:
     ldr r2, [r0]
     orrs r1, r1, r2
     str r1, [r0]
     bx lr


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   led_off_asm
     .type    led_off_asm, %function
/*r0 - base port adress r1 - pin mask*/
led_off_asm:
     ldr r2, [r0]
     bics r2, r1
     str r2, [r0]
     bx lr
     


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   led_off_asm_loop
     .type    led_off_asm_loop, %function 
/*r0 - base port adress r1 - pin mask*/
led_off_asm_loop:
       loop2:
       lsrs r1, 1
       cmp r1, 0
       str r1, [r0]
       bne loop2
       bx lr


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   led_on_asm_mod
     .type    led_on_asm_mod, %function 
/*r0 - base port adress r1 - pin mask*/
led_on_asm_mod:
     ldr r3, =1
     lsls r3, r1
     ldr r2, [r0]
     orrs r3, r3, r2
     str r3, [r0]
     bx lr


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   led_off_asm_mod
     .type    led_off_asm_mod, %function 
/*r0 - base port adress r1 - pin mask*/
led_off_asm_mod:
     ldr r3, =1
     lsls r3, r1
     mvns r3, r3
     ldr r2, [r0]
     ands r3, r3, r2
     str r3, [r0]
     bx lr


     .syntax	unified
     .thumb
     .thumb_func
     .align 2
     .globl   read_input_state
     .type    read_input_state, %function 
/*r0 - base port adress r1 - pin mask return input state on r0*/
read_input_state:
     ldr r2, [r0]
     ands r1, r1, r2
     // cmp r1, 0
     beq label1
     ldr r0, =1
     bx lr
     label1:
     ldr r0, =0
     bx lr 


     .thumb
     .thumb_func
     .align 2
     .globl   chip_select_set
     .type    chip_select_set, %function
/*r0 - base port adress r1 - pin num*/
chip_select_set:
/* отключение подчиненного устройства */
     push {r2, r3, lr}

    push {r0, r1}                 
    ldr r0, =1000                  /* задержка */
    bl delay_asm                
    pop {r0, r1} 

     ldr r3, =1          
     lsls r3, r1         /* сдвигаем 1 на позицию пина */
     mvns r3, r3         /* инвертируем знач. (маска) */
     ldr r2, [r0]        /* загружаем знач. порта */
     ands r3, r3, r2     /* выставляем 0 в позиции нужного пина */
     str r3, [r0]        /* загружаем новое знач. в регистр */

    push {r0, r1}                 
    ldr r0, =100                  /* задержка */
    bl delay_asm                
    pop {r0, r1} 

     pop {r2, r3, pc}


     .thumb
     .thumb_func
     .align 2
     .globl   chip_select_reset
     .type    chip_select_reset, %function
/*r0 - base port adress r1 - pin num*/
chip_select_reset:
/* включение подчиненного устройства */
     push {r2, r3, lr}

    push {r0, r1}                 
    ldr r0, =100                  /* задержка */
    bl delay_asm                
    pop {r0, r1} 

     ldr r3, =1     
     lsls r3, r1         /* сдвигаем 1 на позицию пина */
     ldr r2, [r0]        /* загружаем знач. порта */
     orrs r3, r3, r2     /* выставляем 1 в позиции нужного пина */
     str r3, [r0]        /* загружаем новое знач. в регистр */

     pop {r2, r3, pc}
     .end
