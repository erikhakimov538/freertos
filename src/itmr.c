#include "main.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_timer.h"


#include "itmr.h"

static TIMER_CntInitTypeDef sTIM_CntInit;
static volatile unsigned int TickCnt;

void itmr_Init(void)
{		
	RST_CLK_PCLKcmd(RST_CLK_PCLK_RST_CLK | RST_CLK_PCLK_TIMER2, ENABLE);	
	
	
  /* Reset all TIMER1 settings */
  TIMER_DeInit(MDR_TIMER2);

  TIMER_BRGInit(MDR_TIMER2,TIMER_HCLKdiv4);

  /* TIM1 configuration ------------------------------------------------*/
  /* Initializes the TIMERx Counter ------------------------------------*/
  sTIM_CntInit.TIMER_Prescaler                = 16;		//64
  sTIM_CntInit.TIMER_Period                   = 941;	//1000
  sTIM_CntInit.TIMER_CounterMode              = TIMER_CntMode_ClkFixedDir;
  sTIM_CntInit.TIMER_CounterDirection         = TIMER_CntDir_Up;
  sTIM_CntInit.TIMER_EventSource              = TIMER_EvSrc_None;
  sTIM_CntInit.TIMER_FilterSampling           = TIMER_FDTS_TIMER_CLK_div_1;
  sTIM_CntInit.TIMER_ARR_UpdateMode           = TIMER_ARR_Update_Immediately;
  sTIM_CntInit.TIMER_ETR_FilterConf           = TIMER_Filter_1FF_at_TIMER_CLK;
  sTIM_CntInit.TIMER_ETR_Prescaler            = TIMER_ETR_Prescaler_None;
  sTIM_CntInit.TIMER_ETR_Polarity             = TIMER_ETRPolarity_NonInverted;
  sTIM_CntInit.TIMER_BRK_Polarity             = TIMER_BRKPolarity_NonInverted;
	TIMER_CntInit (MDR_TIMER2,&sTIM_CntInit);
	
	TIMER_ITConfig(MDR_TIMER2, TIMER_STATUS_CNT_ARR, ENABLE);	
	//NVIC_SetPriority(Timer2_IRQn, 3);
	//NVIC_EnableIRQ(Timer2_IRQn);
	
	TIMER_Cmd(MDR_TIMER2, ENABLE);
	
	TickCnt = 0;
}

unsigned int itmr_GetTick(void)
{
	return TickCnt;
}

bool itmr_IsTime(unsigned int stime, unsigned int ival)
{
	return((itmr_GetTick() - stime) >= (ival + 1));
}

void itmr_Delay(unsigned int delay)
{
	unsigned int start = itmr_GetTick();

	while(!itmr_IsTime(start, delay));
}
 
void itmr_AddTickCnt(void)
{
	TickCnt++;
}
