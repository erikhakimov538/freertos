#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "MDR32F9Qx_uart.h"
#include "system_MDR1986BE4.h"
#include "MDR32F9Qx_port.h"
#include "uart.h"
#include "MDR32F9Qx_rst_clk.h"
#include "FreeRTOS.h"
#include "task.h"
#include "MDR1986BE4.h"
#include "MDR32F9Qx_ssp.h"
#include "MCP23S18.h"

#define USE_PIN_NUM 1 
#define USE_PIN 0
#define USE_PORT 0  

uint32_t freq = 0;
uint32_t int_div = 0;
uint32_t frac_div = 0;
volatile uint32_t led_ind = 0;
PORT_InitTypeDef PortInit;
volatile uint32_t counter = 0;

enum States {
    OFF = 0,
    ON = 1
};

enum Place {
    port = 0,
    expander = 1
};

typedef struct leds_desc  {
    uint32_t port;
    uint32_t pin;
    uint32_t pin_number;
    enum States state;
    enum Place place;
} leds_desc_t;

leds_desc_t leds[] = {
    [7] = { .port = MDR_PORTB,
       .pin = PORT_Pin_6,
       .pin_number = 6,
       .state = OFF,
       .place = expander
    },
    [6] = { .port = MDR_PORTA,
       .pin = PORT_Pin_9,
       .pin_number = 9,
       .state = OFF,
       .place = port
    }, 
    [5] = { .port = MDR_PORTA,
       .pin = PORT_Pin_8,
       .pin_number = 8,
       .state = OFF,
       .place = port
    },
    [4] = { .port = MDR_PORTA,
       .pin = PORT_Pin_6,
       .pin_number = 6,
       .state = OFF,
       .place = port
    }, 
    [3] = { .port = MDR_PORTA,
       .pin = PORT_Pin_7,
       .pin_number = 7,
       .state = OFF,
       .place = port
    },
    [2] = { .port = MDR_PORTA,
       .pin = PORT_Pin_10,
       .pin_number = 10,
       .state = OFF,
       .place = port
    },
    [1] = { .port = MDR_PORTB,
       .pin = PORT_Pin_5,
       .pin_number = 5,
       .state = OFF,
       .place = expander
    },
    [0] = { .port = MDR_PORTB,
       .pin = PORT_Pin_4,
       .pin_number = 4,
       .state = OFF,
       .place = expander
    }
};

typedef struct button_desc  {
    uint32_t port;
    uint32_t pin;
} button_desc_t;

button_desc_t btns[] = {
    [0] = { .port = MDR_PORTB,
       .pin = PORT_Pin_9
    },
    [1] = { .port = MDR_PORTB,
       .pin = PORT_Pin_10
    },
    [2] = { .port = MDR_PORTB,
       .pin = PORT_Pin_8
    }, 
    [3] = { .port = MDR_PORTB,
       .pin = PORT_Pin_3
    },
    [4] = { .port = MDR_PORTB,
       .pin = PORT_Pin_2
    }
};

typedef struct com_desc  {
    char command[64];
    uint32_t size;
    uint32_t type;
} commands_t;

commands_t cmds[] = {
    [0] = { .command = {'t','e','s','t'},
       .size = 4,
       .type = 1
    },
    [1] = { .command = {'l','e','d',' ','o','n'},
       .size = 6,
       .type = 2
    },
    [2] = { .command = {'l','e','d',' ','o','f','f'},
       .size = 7,
       .type = 3
    },
    [3] = { .command = {'l','e','d',' ','o','f','f',' ','a','l','l'},
       .size = 11,
       .type = 4
    },
    [4] = { .command = {'l','e','d',' ','o','n',' ','a','l','l'},
       .size = 10,
       .type = 5
    },
    [5] = { .command = {'s','u','m','m','a'},
       .size = 5,
       .type = 6
    },
    [6] = { .command = {'l', 'e', 'd', ' ', 't', 'o', 'g', 'g', 'l', 'e'},
        .size = 10,
        .type = 7
    },
    [7] = { .command = {'l', 'e', 'd', ' ', 't', 'o', 'g', 'g', 'l', 'e', ' ', 'a', 'l', 'l'},
        .size = 14,
        .type = 8
    },
    [8] = { .command = {'d','e','l','t','a'},
       .size = 5,
       .type = 9
    }
};

extern void delay_asm(uint32_t nCount);
extern void led_on_asm(uint32_t port, uint32_t pin);
extern void led_off_asm(uint32_t port, uint32_t pin);
extern void led_on_asm_mod(uint32_t port, uint32_t pin_number);
extern void led_off_asm_mod(uint32_t port, uint32_t pin_number);
extern void led_off_asm_loop(uint32_t port, uint32_t pin);
extern uint32_t read_input_state(uint32_t port, uint32_t pin);
extern void MCP_Init(void);

// Функция задержки
void Delay(uint32_t nCount)  {
    delay_asm(nCount);
}

// Функция зажигания светодиода (для портов А и Б)
void LEDOn_i(uint32_t index)  {
    //PORT_SetBits(leds[index].port, leds[index].pin);
    //led_on_asm(leds[index].port, leds[index].pin);
    // led_on_asm_mod(leds[index].port, leds[index].pin_number);
#if (USE_PIN_NUM == 1)
    led_on_asm_mod(leds[index].port, leds[index].pin_number);
    leds[index].state = ON;

#elif (USE_PIN == 1)

    led_on_asm(leds[index].port, leds[index].pin);

#elif (USE_PORT == 1)

    PORT_SetBits(leds[index].port, leds[index].pin);

#endif
}

// Функция выключения светодиода (для портов А и Б)
void LEDOff_i(uint32_t index)  {
    led_off_asm_mod(leds[index].port, leds[index].pin_number);
    leds[index].state = OFF;
}

// Функция переключения состояния светодиода (для портов А и Б)
void LEDToggle_i(uint32_t index) {
    if (leds[index].state == OFF) {
        LEDOn_i(index);
    }
    else {
        LEDOff_i(index);
    }
}

void LEDToggle_all(void) {
    uint32_t i;
    for (i = 0; i < 5; i++){
        LEDToggle_i(i);
    }
}

// Функция выключения всех светодиодов (для всех светодиодов)
void LEDOff_all(void)  {
    uint32_t i;
    for (i = 0; i < (sizeof(leds)/sizeof(leds_desc_t)); i++)  {
        if (leds[i].place == expander) {
            MCP_Set_Pin(leds[i].pin_number, 0);
        } else {
            LEDOff_i(i);
        }
    }
}

void LEDOn_all(void)  {
    uint32_t i;
    for (i = 0; i < 5; i++)  {
        LEDOn_i(i);
    }
}

void SysTick_ISR(void) {
    counter++;
}

// Функция моргания светодиодом (для всех светодиодов)
void BlinkLED_All(void)  {
    while(1)  {
        led_ind = led_ind%(sizeof(leds)/sizeof(leds_desc_t));
        if (leds[led_ind].place == expander) {
            MCP_Set_Pin(leds[led_ind].pin_number, 1);
            Delay(1000000);
        } else {
            LEDOn_i(led_ind);
            Delay(1000000);
        }
        LEDOff_all();
        Delay(1000000);
        vTaskDelay(200/portTICK_PERIOD_MS);
    }
}

// Функция проверки нажатия кнопок
void ButtonCheck(void) {
    uint8_t button_status;
    uint8_t button_status2;
    static uint32_t index_button = 0;
    static uint32_t index_button2 = 1;
    static uint32_t state = 1;
    static uint32_t state2 = 1;

    while(1)    {
        button_status = read_input_state(btns[index_button].port, btns[index_button].pin);
        button_status2 = read_input_state(btns[index_button2].port, btns[index_button2].pin);

        if (button_status == 0)  {
            if (state != button_status)  { 
                state = button_status;
                ++led_ind;
        }
        } else if (button_status2 == 0)  {
            if (state2 != button_status2)  { 
                state2 = button_status2;
                --led_ind;
            }
        }
        else  {
            state = button_status;
            state2 = button_status2;
        }
        vTaskDelay(200/portTICK_PERIOD_MS);
    }        
}

// Функция настройки кнопок
void ButtonGfg (int index)  {
    PortInit.PORT_Pin   = (btns[index].pin);
    PortInit.PORT_OE    = PORT_OE_IN ;
    PortInit.PORT_FUNC  = PORT_FUNC_PORT;
    PortInit.PORT_MODE  = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_OUTPUT_OFF;

    PORT_Init(btns[index].port, &PortInit);
}

// Функция настройки светодиодов
void LedGfg (int index)  {
    PortInit.PORT_Pin   = (leds[index].pin);
    PortInit.PORT_OE    = PORT_OE_OUT;
    PortInit.PORT_FUNC  = PORT_FUNC_PORT;
    PortInit.PORT_MODE  = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_SPEED_SLOW;

    PORT_Init(leds[index].port, &PortInit);
}

// Функция выполнения команд
void Commander(char *buffer)  {
    uint32_t i;
    uint32_t x;
    float a;
    float b;
    bool cmd_success = false;

    for (i =0; i < sizeof(cmds)/sizeof(commands_t); i++)  {
        if (strncmp(buffer, cmds[i].command, cmds[i].size) == 0) {
            switch(cmds[i].type)  {
                case 1 :
                    uart_SendStr("Test OK \r\n");
                    cmd_success = true;
                    break;
                case 2 :
                    if(isdigit(buffer[7])) {
                        sscanf(&buffer[6], "%d", &x);
                        LEDOn_i(x);
                        uart_SendStr("LEDOn_i OK \r\n");
                        cmd_success = true;
                    }
                    break;
                case 3 :
                    if(isdigit(buffer[8])) {
                        sscanf(&buffer[7], "%d", &x);
                        LEDOff_i(x);
                        uart_SendStr("LEDOff_i OK \r\n");
                        cmd_success = true;  
                    }
                    break;
                case 4 :
                    uart_SendStr("LEDOff_all OK \r\n");
                    LEDOff_all();
                    cmd_success = true;
                    break;
                case 5 :
                    uart_SendStr("LEDOn_all OK \r\n");
                    LEDOn_all();
                    cmd_success = true;
                    break;
                case 6 :
                    uart_SendStr("Summa OK \r\n");
                    sscanf(&buffer[5], "%f %f", &a, &b);
                    a = a + b;
                    memset(buffer, 0, 64);
                    snprintf(buffer, 64, "Summa: %f\r\n", a);
                    uart_SendStr(buffer);
                    cmd_success = true;
                    break;
                case 7 :
                    if (isdigit(buffer[12])) {
                        uart_SendStr("LEDToggle_i OK \r\n");
                        sscanf(&buffer[11], "%d", &x);
                        LEDToggle_i(x);
                        cmd_success = true;
                    }
                    break;
                case 8 :
                    uart_SendStr("LEDToggle_i OK \r\n");
                    LEDToggle_all();
                    cmd_success = true;
                    break;
                case 9 :
                    uart_SendStr("Delta OK \r\n");
                    sscanf(&buffer[5], "%f %f", &a, &b);
                    a = a - b;
                    memset(buffer, 0, 64);
                    snprintf(buffer, 64, "Delta: %f\r\n", a);
                    uart_SendStr(buffer);
                    cmd_success = true;
                    break;
                default :
                    uart_SendStr("Unknown command \r\n");
                    break;
            }
            if (cmd_success == true) {
                break;
            }
        }
    }
        if (i == sizeof(cmds)/sizeof(commands_t)) {
            uart_SendStr("Unknown command \r\n");
        }
    memset(buffer,0,64);
}

void vApplicationMallocFailedHook( void )   {
    return;
}

// Функция отправки данных по SPI
void SPI_Send_Data(uint16_t Data)    {
    SSP_SendData(MDR_SSP1_BASE, Data);
    while (RESET) {
        SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_BSY);
    }
    SSP_ReceiveData(MDR_SSP1_BASE);
}

// Функция настройки SPI
void SPI_Init (void) {
    PORT_InitTypeDef PortInit;
    SSP_InitTypeDef  sSSP;

    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_SSP1, ENABLE);

    PortInit.PORT_Pin = PORT_Pin_1 | PORT_Pin_13 | PORT_Pin_15;
    PortInit.PORT_OE = PORT_OE_OUT;
    PortInit.PORT_FUNC = PORT_FUNC_MAIN;
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_PD = PORT_PD_DRIVER;
    PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTA, &PortInit); 

    PortInit.PORT_Pin = PORT_Pin_14;
    PortInit.PORT_OE = PORT_OE_IN;
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_PD = PORT_PD_DRIVER;
    PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTA, &PortInit); 

    SSP_BRGInit(MDR_SSP1_BASE, SSP_HCLKdiv1);

    SSP_StructInit(&sSSP);

    sSSP.SSP_CPSDVSR = 2;
    sSSP.SSP_SCR = 0x00;
    sSSP.SSP_Mode = SSP_ModeMaster;
    sSSP.SSP_WordLength = SSP_WordLength8b;
    sSSP.SSP_SPO = SSP_SPO_Low;
    sSSP.SSP_SPH = SSP_SPH_1Edge;
    sSSP.SSP_FRF = SSP_FRF_SPI_Motorola;
    sSSP.SSP_HardwareFlowControl = SSP_HardwareFlowControl_None;
    SSP_Init(MDR_SSP1_BASE, &sSSP);

    SSP_Cmd(MDR_SSP1_BASE, ENABLE);
}

int main(void)  {

    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTC, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_SSP1, ENABLE);

    SysTick_Config(16000000);

    uint32_t i;
    uint32_t letter_num = 0;
    char res;
    char sep [10]=" ";
    char command;
    char letters[] = {0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x0D, 0x0A};
    char buffer[64];

    // Настройка кнопок и светодиодов
    for (i =0; i < sizeof(leds)/sizeof(leds_desc_t); i++)  {
        LedGfg(i);
    }

    for (i =0; i < sizeof(btns)/sizeof(button_desc_t); i++)  {
        ButtonGfg(i);
    }

    // Инициализация SPI, MCP23, UART
    uart_Init(115200, UART_WordLength8b, UART_StopBits1, UART_Parity_No);
    SPI_Init();
    MCP_Init();

    // Создание задач и запуск планировщика
    xTaskCreate(&ButtonCheck, "Check_button", 200, NULL, 1, NULL);
    xTaskCreate(&BlinkLED_All, "Blink2", 50, NULL, 1, NULL);
    vTaskStartScheduler();

    while(1)  {
        // TapButtonFireLed(0, 1);
        // Delay(10000);
  }
}

#pragma clang diagnostic pop