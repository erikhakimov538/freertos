#include "MCP23S18.h"

static PORT_InitTypeDef PortInit;

extern void chip_select_reset(uint32_t port, uint32_t pin);
extern void chip_select_set(uint32_t port, uint32_t pin);
extern void SPI_Send_Data(uint16_t Data);

// Функция настройки пинов MCP
void MCP_Pins_Init(void)   {
    PortInit.PORT_FUNC = PORT_FUNC_PORT;
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_PULL_UP = PORT_PULL_UP_OFF;
    PortInit.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
    PortInit.PORT_PD_SHM = PORT_PD_SHM_OFF;
    PortInit.PORT_PD = PORT_PD_DRIVER;
    PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;
    PortInit.PORT_GFEN = PORT_GFEN_OFF;

    PortInit.PORT_Pin = PORT_Pin_1;
    PortInit.PORT_OE = PORT_OE_OUT;
    PORT_Init(MDR_PORTA, &PortInit); 
}

// Функция отправки данных
void MCP_Send_Data(uint32_t Register, uint16_t Data)    {
    chip_select_set(MDR_PORTA_BASE, 1);
    SPI_Send_Data(0x40);
    SPI_Send_Data(Register);
    SPI_Send_Data(Data);
    chip_select_reset(MDR_PORTA_BASE, 1);
    Delay(100);
}

// Функция инициализации MCP
void MCP_Init(void) {
    MCP_Pins_Init();
    MCP_Send_Data(IOCON_default, INTCONB_cfg);
    MCP_Send_Data(DIRB, DIRB_cfg);
    MCP_Send_Data(IPOLB, IPOLB_cfg);
    MCP_Send_Data(GPINTENB, GPINTENB_cfg);
    MCP_Send_Data(DEFVALB, DEFVALB_cfg);
    MCP_Send_Data(INTCONB, INTCONB_cfg);
    MCP_Send_Data(GPPUB, GPPUB_cfg);
}

// Функция переволда состояния пина (для моргания светодиодами)
void MCP_Set_Pin (uint32_t pin_num, uint32_t state) {
    uint32_t tmp_data;
    
    if (state != 0) {
        tmp_data = 1 << pin_num;    
    } else {
        tmp_data = 0;
    }
    MCP_Send_Data(GPIOB, tmp_data); 
}

